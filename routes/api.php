<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return response('Test API', 200)
        ->header('Content-Type', 'application/json');
});


Route::get('/task/{id}', function () {
    return response('Test API', 200)
        ->header('Content-Type', 'application/json');
});

Route::get('/task/{id}/assignee/toggle', function () {
    return response('Test API', 200)
        ->header('Content-Type', 'application/json');
})->name('api.task.assignee.toggle');

    //->middleware('auth');

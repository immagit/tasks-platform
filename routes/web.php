<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/my/jobs', 'HomeController@myJobs')->name('home.my_jobs');

Route::resources([
    'task' => 'TasksController',
    'comment' => 'CommentController',
]);


Route::group(['middleware' => 'check_admin'], function()
{
    Route::get('drafts', 'HomeController@drafts')->name('task.drafts')->middleware('check_admin');
    Route::get('closed', 'HomeController@closed')->name('task.closed')->middleware('check_admin');
    Route::get('assigned', 'HomeController@assigned')->name('task.assigned')->middleware('check_admin');

    Route::resources([
        'wiki' => 'WikiController',
    ]);
});



Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');


Route::get('login/facebook', 'Auth\LoginController@redirectFacebook')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

Route::get('login/twitter', 'Auth\LoginController@redirectTwitter')->name('login.twitter');
Route::get('login/twitter/callback', 'Auth\LoginController@handleTwitterCallback');

Route::get('login/google', 'Auth\LoginController@redirectGoogle')->name('login.google');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');


Route::get('/task/{id}/assignee/toggle', 'TasksController@assigneeToggle')
    ->name('task.assignee.toggle')
    ->middleware('auth');


if (config('app.env') == 'development') {
    Route::get('/test2', 'HomeController@test2');
    Route::get('/test', function(){

        $task = \App\Task::find(3);
        $user = \Illuminate\Support\Facades\Auth::user();

        \App\Mail::to(User::find(1))
            ->send(new CommentSent(\App\Comment::find(1)));

        var_dump('');echo "web.php61";exit;
        $c = \App\Comment::find(1);
        $m = new \App\Mail\CommentSent($c);
        $m->build();

//    $task->assignee()->detach($user);
//    $task->assignee()->attach($user);
        $task->assignee()->toggle($user);

        $ass = $task->assignee()->get();

        foreach ($ass as $item) {
            var_dump($item->email);
        }
//    $user->roles()->detach($roleId);
//    $user->roles()->attach($roleId, ['expires' => $expires]);

        return 'end';
    });
}

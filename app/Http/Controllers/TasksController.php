<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CreateArticleRequest;
use App\Task;
use App\Wiki;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TasksController extends Controller
{

    public function __construct()
    {
        $this->middleware('check_admin', ['except' => ['show', 'assigneeToggle']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $task = new Task();
        $task->price = Task::DEFAULT_PRICE;

        $data = session('create_task');

        if ($data) {
            $task->fill($data->toArray());
        }

        $wikies = Wiki::get();

        return view('tasks.create')->with(compact('task', 'wikies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticleRequest $request)
    {
        $task = Task::create($request->all());

        session()->forget('create_task');
        session()->flush();

        return redirect()->route('task.show', ['id' => $task->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answers = [];
        $task = Task::findOrFail($id);

        if (Auth::check()) {

            if ($task->status !== 'open'
                and !$task->assignee()->find(Auth::user()->id)
                and !Auth::user()->isAdmin()) {
                    abort(404);
            }

            $task->with('comments');
            foreach ($task->commentAnswers()->with('owner')->get() as $comment) {
                if (!isset($answers[$comment->parent_id])) {
                    $answers[$comment->parent_id] = [];
                }
                $answers[$comment->parent_id][] = $comment;
            }
        } else {
            if ($task->status !== 'open') {
                abort(404);
            }
        }

        return view('tasks.show', [
            'task'      => $task,
            'answers'   => $answers,
            'comments'  => $task->comments
        ]);
    }

    /**
     * @param $id
     */
    public function assigneeToggle($id)
    {
        $task = Task::findOrFail($id);

        $task->assignee()->toggle(Auth::user());
        return redirect(route('task.show', ['id' => $task->id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $wikies = Wiki::get();

        return view('tasks.create')->with(compact('task', 'wikies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        $task->fill($request->all());
        $task->save();

        return redirect()->route('task.show', ['id' => $task->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

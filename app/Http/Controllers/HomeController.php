<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\CommentSent;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::where('status', 'open')->get();

        return view('tasks.list', [
            'tasks' => $tasks
        ]);
    }

    public function drafts()
    {
        $tasks = Task::where('status', 'draft')->orderBy('id')->get();

        return view('tasks.list', [
            'tasks' => $tasks
        ]);
    }


    public function closed()
    {
        $tasks = Task::where('status', 'closed')->get();

        return view('tasks.list', [
            'tasks' => $tasks
        ]);
    }

    public function assigned()
    {
        $tasks = Task::where('status', 'open')
            ->has('assignee', '>', 0)->get();

        return view('tasks.list', [
            'tasks' => $tasks
        ]);
    }

    public function myJobs()
    {
        return view('tasks.list', [
            'tasks' => Auth::user()->tasks
        ]);
    }

    public function test2()
    {
        Mail::to(User::find(1))
            ->send(new CommentSent(Comment::find(1)));
    }
}

<?php

namespace App\Http\Requests;

use App\Task;
use Illuminate\Foundation\Http\FormRequest;

class CreateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }


    protected function validationData()
    {
        $all = parent::validationData();

        $data = collect($all)->only('title', 'overview', 'text', 'price', 'deadline');
        session(['create_task' => $data]);

        return $all;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => Task::VAL_TITLE,
            'text'      => Task::VAL_TEXT,
            'overview'  => Task::VAL_OVERVIEW,
            'deadline'  => 'required|date',
            'status'    => 'required'
        ];
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $attributes = [

    ];

    protected $casts = [
//        'tags' => 'array'
    ];

    protected $fillable = [
        'text'
    ];

    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    public function parent()
    {
        return $this->belongsTo('App\Comment');
    }

    public function owner()
    {
        return $this->belongsTo('App\User');
    }
}

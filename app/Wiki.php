<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wiki extends Model
{
    const VAL_TITLE = 'required';
    const VAL_LINK = 'required';

    protected $fillable = [
        'title',
        'link',
    ];

    //
}

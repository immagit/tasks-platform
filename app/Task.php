<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    const VAL_TITLE = 'required';
    const VAL_TEXT = 'required';
    const VAL_OVERVIEW = 'required';

    const DEFAULT_PRICE = 10;

    protected $dates = ['deadline'];

    protected $attributes = [
        'status'    => 'draft',
        'price'     => self::DEFAULT_PRICE,
        'tags'      => 'php Laravel'
    ];

    protected $casts = [
//        'tags' => 'array'
    ];

    protected $fillable = [
        'title',
        'text',
        'footer',
        'overview',
        'tags',
        'price',
        'deadline',
        'status',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here

        });

        self::creating(function($model){
            // ... code here

//            $model->tags = explode(' ', $model->tags);
        });

        self::updating(function($model){
            // ... code here

//            $model->tags = explode(' ', $model->tags);
        });
    }


    public function getFormattedDeadlineAttribute()
    {
        return Carbon::parse($this->deadline)->format('M d Y');
    }

    public function getHoursLeftAttribute()
    {
        return Carbon::now()->diffInHours(
            Carbon::parse($this->deadline)
        );
    }

    /**
     * Get the phone record associated with the user.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment')->whereNull('parent_id');
    }

    public function commentAnswers()
    {
        return $this->hasMany('App\Comment')->whereNotNull('parent_id');
    }

    public function assignee()
    {
        return $this->belongsToMany('App\User');
    }

    public function getFormattedText()
    {
        $ret = [];
        $sum = '';

        $Parsedown = new \Parsedown();

        foreach (explode("\r\n", $this->text) as $item) {

            if (strpos($item, '===') === 0) {

                $matches = [];
                preg_match_all('/(\d{1,})/', $item, $matches);

                if (isset($matches[1])) {
                    $wikies = Wiki::whereIn('id', $matches[1])->get();

                    $sum .= '<p class="links-holder">Wiki: ';
                    foreach ($wikies as $wiki) {
                        $sum .= ' <a target="_blank" href="'.$wiki->link.'">'.$wiki->title.'</a> ';
                    }
                    $sum .= '</p>';
                }

            } else {

                if (strlen($item)>1) {

                    $sum .= $item;

                } else {
                    if (strlen($sum) > 1) {
                        $ret[] = $Parsedown->text($sum);
                        $sum = '';
                    }
                }
            }
        }

        if (strlen($sum) > 1) {
            $ret[] = $Parsedown->text($sum);
        }

        return $ret;
    }

    /**
     * @return string
     */
    public function getShortTextAttribute()
    {
        $ret = [];
        foreach (explode("\r\n", $this->text) as $item) {
            if (strpos($item, '===') === 0)
            {
                continue;
            }
            $ret[] = $item;
        }

        return implode("\r\n", $ret);
    }

    public function getIsAssignedAttribute()
    {
        if (!Auth::check()) {
            return false;
        }

        return $this->assignee()->find(Auth::user()->id) !== null;
    }
}

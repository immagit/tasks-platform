<div class="item comments-handler">
    <h4 class="post-title slide-title">Comments</h4>

    @foreach ($comments as $comment)
        <article class="comment">
            <div class="col-md-12 col-sm-12 mb-3">
                <div class="carousel-caption">
                    <div class="post-meta">
                        <span>{{ $comment->owner->name ?: $comment->owner->email}}</span>
                        <span style="font-size: 11px"> - <a href="">{{$comment->created_at}}</a></span>

                        <span>
                            <a href="javascript:void(0)" class="reply" title="Reply to this thread">
                                <i class="fa fa-reply post-meta-icon"></i> Reply
                            </a>
                        </span>
                    </div>

                    <div class="post-content no-border">
                        <p>{{$comment->text}}</p>
                    </div>
                </div>

                @php
                    $subComments = isset($answers[$comment->id]) ? $answers[$comment->id] : [];
                @endphp

                @foreach ($subComments as $answer)
                    <div class="carousel-caption comment-answer">
                        <div class="post-meta">
                            <span>{{ $answer->owner->name ?: $answer->owner->email}}</span>
                            {{--<span style="font-size: 11px"> - <a href="">{{$answer->created_at}}</a></span>--}}
                        </div>

                        <div class="post-content no-border">
                            <p>{{$answer->text}}</p>
                        </div>
                    </div>
                @endforeach

                <div class="email-section mb-7" style="display: none;">
                    <form action="{{route('comment.store')}}" method="POST">
                        @csrf
                        <input type="hidden" value="{{ $task->id }}" name="taskId">
                        <input type="hidden" value="{{ $comment->id }}" name="replyId">
                        <textarea name="text" class="form-control" rows="3">comment</textarea>
                        <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-paper-plane"></i></button>
                    </form>
                </div>

                <hr>
            </div>
        </article>
    @endforeach

    <div class="col-md-12 col-sm-12 email-section" >
        <form id="new-comment-form" action="{{route('comment.store')}}" method="POST">
            @csrf
            <input type="hidden" value="{{ $task->id }}" name="taskId">
            <textarea name="text" class="form-control" rows="3" placeholder="Feel free to ask"></textarea>
            <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-paper-plane"></i></button>
        </form>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {
        var $box = $('.comments-handler');

        $('.reply', $box).click(function() {
            $('.email-section', $(this).parents('.comment')).toggle();
        })
    });

</script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>7nly - jobs</title>

    <!-- Bootstrap -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="/css/offcanvas.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <header>
        <!--========================== Header-Top ================================-->
        {{--<div class="header-top">--}}
            {{--<div class="container">--}}
                {{--<div class="col-md-9 col-sm-7 xs-view">--}}
                    {{--<a href="index.html"><img class="logo" src="images/logo.png" alt="Logo"/></a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-5 xs-view-right">--}}
                    {{--<!-- Author -->--}}
                    {{--<div class="author-form">--}}
                        {{--<li class="dropdown">--}}
                            {{--<form>--}}
                                {{--<a href="#" class="dropdown-toggle author-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--<i class="fa fa-user author-icon"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                    {{--<!-- Nav tabs -->--}}
                                    {{--<ul class="nav nav-tabs" role="tablist">--}}
                                        {{--<li role="presentation" class="active">--}}
                                            {{--<a class="sign" href="#signin" aria-controls="signin" role="tab" data-toggle="tab">sign in</a>--}}
                                        {{--</li>--}}
                                        {{--<li role="presentation">--}}
                                            {{--<a class="sign" href="#signup" aria-controls="signup" role="tab" data-toggle="tab">sign up</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}

                                    {{--<!-- Tab panes -->--}}
                                    {{--<div class="tab-content sign-form">--}}
                                        {{--<div role="tabpanel" class="tab-pane active" id="signin">--}}
                                            {{--<ul class="user-dropdown">--}}
                                                {{--<div class="login-area">--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputText1">Username</label>--}}
                                                        {{--<input type="email" class="form-control" id="exampleInputEmail1">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputPassword1">Password</label>--}}
                                                        {{--<input type="password" class="form-control" id="exampleInputPassword1">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="checkbox">--}}
                                                    {{--<input id="option" type="checkbox" name="field" value="option">--}}
                                                    {{--<label for="option"><span><span></span></span>Keep me singed in</label>--}}
                                                    {{--<a href="#" class="pull-right">Forgot?</a>--}}
                                                {{--</div>--}}
                                                {{--<div class="form-submit">--}}
                                                    {{--<button type="submit" class="btn btn-success btn-block">Sign in</button>--}}
                                                {{--</div>--}}
                                            {{--</ul><!-- /User-Dropdown-->--}}
                                        {{--</div><!-- /#Sing in -->--}}

                                        {{--<div role="tabpanel" class="tab-pane" id="signup">--}}
                                            {{--<ul class="user-dropdown">--}}
                                                {{--<div class="login-area">--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputText1">Username</label>--}}
                                                        {{--<input type="email" class="form-control" id="exampleInputEmail1">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputEmail1">Email Address</label>--}}
                                                        {{--<input type="email" class="form-control" id="exampleInputEmail1">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputPassword1">Password</label>--}}
                                                        {{--<input type="password" class="form-control" id="exampleInputPassword1">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="exampleInputPassword1">Comfirm Password</label>--}}
                                                        {{--<input type="password" class="form-control" id="exampleInputPassword1">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="checkbox">--}}
                                                    {{--<input id="option1" type="checkbox" name="field" value="option1">--}}
                                                    {{--<label for="option1"><span><span></span></span>I accept Terms and Condition ?</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="form-submit">--}}
                                                    {{--<button type="submit" class="btn btn-success btn-block">Sign up</button>--}}
                                                {{--</div>--}}
                                            {{--</ul><!-- /User-Dropdown-->--}}
                                        {{--</div><!-- /#Sing up -->--}}
                                    {{--</div><!-- /Tab-Content -->--}}
                                {{--</ul><!-- /Dropdown-menu -->--}}
                            {{--</form>--}}
                        {{--</li><!-- /Dropdown -->--}}
                    {{--</div><!-- /Author -->--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div><!-- header-top -->--}}

        <!--========================== Header-Nav ================================-->
        <div class="header-nav">
            <nav class="navbar navbar-default">
                <div class="container">
                    {{--<p class="pull-left visible-xs">--}}
                        {{--<button type="button" class="navbar-toggle" data-toggle="offcanvas">--}}
                            {{--<i class="fa fa-long-arrow-right"></i>--}}
                            {{--<i class="fa fa-long-arrow-left"></i>--}}
                        {{--</button>--}}
                    {{--</p>--}}

                    {{--<div class="social-nav center-block visible-xs">--}}
                        {{--<li><a href="#"><i class="fa fa-twitter twitter"></i></a></li>--}}
                        {{--<li><a href="#"><i class="fa fa-facebook facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="fa fa-google-plus google-plus"></i></a></li>--}}
                    {{--</div>--}}
                    <!--toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-left">

                            <li id="logo-item">
                                <a href="{{ route('home') }}">
                                    <img class="logo" src="/img/logo3.png" alt="">
                                </a>
                            </li>

                            @auth
                                @if (Auth::user()->isAdmin())
                                    <li class="{{ active('home') }}"><a href="{{ route('home') }}">home</a></li>
                                    <li class="{{ active('task.create') }}"><a href="{{ route('task.create') }}">Create task</a></li>
                                    <li class="{{ active('task.drafts') }}"><a href="{{ route('task.drafts') }}">Drafts</a></li>
                                    <li class="{{ active('task.closed') }}"><a href="{{ route('task.closed') }}">Closed tasks</a></li>
                                    <li class="{{ active('task.assigned') }}"><a href="{{ route('task.assigned') }}">Assigned tasks</a></li>
                                @endif

                                <li class="{{ active('home.my_jobs') }}"><a href="{{ route('home.my_jobs') }}">My jobs</a></li>
                                <li><a href="/logout">Logout</a></li>
                                <li><a href="#">{{ Auth::user()->name }}</a></li>
                            @endauth

                        </ul>
                            {{--<li><a href="#"><i class="fa fa-twitter twitter"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-facebook facebook"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-google-plus google-plus"></i></a></li>--}}
                        {{--</ul>--}}

                        {{--<div class="col-md-2 search-section center-block hidden-xs">--}}
                            {{--<form>--}}
                                {{--<input type="text" class="form-control" id="exampleInputName2" placeholder="Search">--}}
                                {{--<button type="submit" class="btn btn-default btn-xs"></button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-->
            </nav>
        </div><!-- Header-Nav -->
    </header>
    <!--========================== Contant-Area================================-->
    <div class="contant-area">
        <div class="container">
            @yield('content')
        </div><!-- Container -->
    </div><!-- Content-area -->
    <footer>
        {{--<div class="footer-menu">--}}
            {{--<div class="container">--}}
                {{--<div class="col-md-4 col-sm-4 center-block">--}}
                    {{--<h3 class="footer-head">this author blog from crea tivemine</h3>--}}
                    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
                    {{--<div class="social">--}}
                        {{--<li><a href="#"><i class="fa fa-facebook facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="fa fa-twitter twitter"></i></a></li>--}}
                        {{--<li><a href="#"><i class="fa fa-google-plus google-plus"></i></a></li>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 mobile-footer center-block">--}}
                    {{--<h4 class="footer-head">recent posts</h3>--}}
                        {{--<ul class="ul-left">--}}
                            {{--<li><a href="#">Ut enim ad minim veniam</a></li>--}}
                            {{--<li><a href="#">Quis nostrud exercitation laboris</a></li>--}}
                            {{--<li><a href="#">Duis aute irure dolor reprehenderit</a></li>--}}
                            {{--<li><a href="#">Excepteur sint occaecat non</a></li>--}}
                            {{--<li><a href="#">Proident,sunt in culpa qui officia</a></li>--}}
                            {{--<li><a href="#">Accusantium doloremque</a></li>--}}
                        {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 mobile-footer center-block">--}}
                    {{--<h4 class="footer-head">article categories</h3>--}}
                        {{--<ul class="ul-left">--}}
                            {{--<li><a href="#">Academy</a></li>--}}
                            {{--<li><a href="#">Blogging</a></li>--}}
                            {{--<li><a href="#">Conversion</a></li>--}}
                            {{--<li><a href="#">Design</a></li>--}}
                            {{--<li><a href="#">E-Commerce</a></li>--}}
                            {{--<li><a href="#">Facebook</a></li>--}}
                            {{--<li><a href="#">Infographics</a></li>--}}
                            {{--<li><a href="#">Launch</a></li>--}}

                        {{--</ul>--}}
                        {{--<ul class="ul-right">--}}
                            {{--<li><a href="#">Strategies</a></li>--}}
                            {{--<li><a href="#">Marketing</a></li>--}}
                            {{--<li><a href="#">SEO</a></li>--}}
                            {{--<li><a href="#">Social</a></li>--}}
                            {{--<li><a href="#">Media</a></li>--}}
                            {{--<li><a href="#">Testing</a></li>--}}
                            {{--<li><a href="#">Twitter</a></li>--}}
                        {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div><!-- Footer-menu -->--}}
        {{--<div class="footer-nav">--}}
            {{--<div class="container">--}}
                {{--<div class="col-md-6 col-sm-5">--}}
                    {{--<p>&copy; 2015 Author bloging template</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-6 col-sm-7">--}}
                    {{--<ul>--}}
                        {{--<li><a href="index.html">home</a></li>--}}
                        {{--<li><a href="#">support</a></li>--}}
                        {{--<li><a href="#">careers</a></li>--}}
                        {{--<li><a href="#">terms use</a></li>--}}
                        {{--<li><a href="#">privacy policy</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Go TO TOP -->--}}
            <div id="toTop" class="btn btn-info" style="display: block;">
                <span class="fa fa-angle-up"></span>
            </div><!-- /Go-to-top -->
        {{--</div>--}}
    </footer>
</div><!-- /Wrapper -->

<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src ="/js/custom.js"></script>

@yield('js')

</body>
</html>
@extends('layout')

@section('content')

    <div class="row row-offcanvas row-offcanvas-left tasks-list">
        @foreach ($tasks as $task)

            <div class="col-md-4 col-sm-6 col-xs-12 cart">
                <div class="main-content">
                    <article>
                        <a href="{{ route('task.show', ['id' => $task->id]) }}">
                            <h4 class="post-title">{{ $task->title }}</h4>
                        </a>

                        <div class="post-meta">
                            <div>
                                <span>
                                    <b>{{$task->price}}$</b>
                                </span>

                                @foreach(explode(' ', $task->tags) as $tag)
                                    <a href="#" class="tag btn btn-default btn-xs" type="button">{{ $tag }}</a>
                                @endforeach
                            </div>

                            <div class="mt-1">
                                <i class="fa fa-calendar-check-o post-meta-icon"></i>
                                {{$task->hours_left}} hours left ({{ $task->formatted_deadline }})
                            </div>
                        </div>

                        @include('tasks.admin-menu', ['task' => $task])

                        <div class="post-content">
                            {{ $task->short_text }}
                        </div>

                    </article>
                </div>
            </div>

        @endforeach
    </div>

@endsection


@extends('layout')

@section('content')
<div class="row row-offcanvas row-offcanvas-left">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <a href="/">All jobs</a> / This one
    </div>
</div>

<div class="row row-offcanvas row-offcanvas-left">

    <div class="col-md-7 col-sm-12 col-xs-12">
        <div class="main-content">
            <article>
                <h3 class="post-title mt-1">
                    {{ $task->title }}
                    @auth
                        @if(Auth::user()->isAdmin() and $task->status != 'open')
                           ({{$task->status}})
                        @endif
                    @endauth
                </h3>

                <hr>

                <div class="post-content2">
                    {{ $task->overview }}
                </div>

                <h4 class="mt-3">Scope:</h4>

                @foreach($task->getFormattedText() as $issue)
                    <div class="post-content">{!! $issue !!}</div>
                @endforeach

                <h4 class="mt-3">Submission Guidelines:</h4>
                <div class="post-content">{!! $task->footer !!}</div>

            </article>

        </div><!-- main-content -->
    </div>

    <!--========================== Right-Sidebar ================================-->

    <div class="col-md-5 col-sm-12 col-xs-12">
        <div class="right-sidebar">
            <div class="righ-sidebar-body">

                <div class="post-meta">
                    <div class="mt-2">
                        <i class="fa fa-dollar-sign"></i>Compensation: <b>{{ $task->price }}$</b>
                    </div>

                    <div class="mt-2">
                        <i class="fa fa-calendar-check-o post-meta-icon"></i> <b>{{ $task->hours_left }} hours</b> left to deadline ({{ $task->formatted_deadline }})
                    </div>

                    <div class="mt-2">
                        @foreach(explode(' ', $task->tags) as $tag)
                            <a href="#" class="tag btn btn-default btn-xs" type="button">{{ $tag }}</a>
                        @endforeach
                    </div>
                </div>

                @include('tasks.admin-menu', ['task' => $task])

                <div class="task-register mt-1">
                    <a href="{{route('task.assignee.toggle', ['id' => $task->id])}}" class="btn btn-success btn-block @guest disabled @endguest">
                        @if($task->isAssigned)
                            Stop work
                        @else
                            Start work
                        @endif
                    </a>
                </div>

                <hr>

                @guest
                <div class="register-block">
                    <h5 class="text-center">To start work, please login/register first</h5>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a class="sign" href="#signin" aria-controls="signin" role="tab" data-toggle="tab">sign in</a>
                        </li>
                        <li role="presentation">
                            <a class="sign" href="#signup" aria-controls="signup" role="tab" data-toggle="tab">sign up</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="signin">
                            <ul class="user-dropdown">
                                <form action="/login" method="POST">
                                    @csrf
                                    <input type="hidden" name="taskId" value="{{ $task->id }}">

                                    <div class="login-area">
                                        <div class="form-group">
                                            <label for="exampleInputText1">Email</label>
                                            <input type="email" name="email" class="form-control">
                                            @if ($errors->has('email'))
                                                <div class="invalid-feedback alert alert-danger">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" name="password" class="form-control">

                                            @if ($errors->has('password'))
                                                <div class="invalid-feedback alert alert-danger">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input id="option" type="checkbox" name="remember" value="option">
                                        <label for="option"><span><span></span></span>Keep me singed in</label>
                                        <a href="/password/reset" class="pull-right">Forgot?</a>
                                    </div>
                                    <div class="form-submit">
                                        <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                    </div>
                                </form>
                            </ul><!-- /User-Dropdown-->
                        </div><!-- /#Sing in -->
                        <div role="tabpanel" class="tab-pane" id="signup">
                            <ul class="user-dropdown">
                                <form action="/register" method="POST">
                                    @csrf
                                    <input type="hidden" name="taskId" value="{{ $task->id }}">

                                    <div class="login-area">
                                        <div class="form-group">
                                            <label for="exampleInputText1">Username</label>
                                            <input type="text" name="name" class="form-control" id="exampleInputEmail1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email Address</label>
                                            <input type="email" name="email" class="form-control" id="exampleInputEmail1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input id="option1" type="checkbox" name="field" value="option1">
                                        <label for="option1"><span><span></span></span>I accept Terms and Condition ?</label>
                                    </div>
                                    <div class="form-submit">
                                        <button type="submit" class="btn btn-success btn-block">Sign up</button>
                                    </div>
                                </form>
                            </ul><!-- /User-Dropdown-->
                        </div><!-- /#Sing up -->
                    </div><!-- /Tab-Content -->

                    <ul class="social-nav center-block mt-5">
                        <li><a href="{{ route('login.twitter') }}"><i class="fa fa-twitter twitter"></i></a></li>
                        <li><a href="{{ route('login.facebook') }}"><i class="fa fa-facebook facebook"></i></a></li>
                        <li><a href="{{ route('login.google') }}"><i class="fa fa-google-plus google-plus"></i></a></li>
                    </ul>
                </div>
                @endguest

                @auth
                    @if($task->isAssigned or Auth::user()->isAdmin())
                        @include('comments.list', [
                            'task' => $task,
                            'comments' => $task->comments,
                            'answers' => $answers,
                        ])
                    @endif
                @endauth
            </div><!-- Righ-sidebar-body -->
        </div><!-- Right-Sidebar -->
    </div>

</div>

@endsection

@section('js')
    <script type="text/javascript">
        var from_show = 1;

        $(document).ready(function() {
            // debugger;
        });

    </script>
@endsection

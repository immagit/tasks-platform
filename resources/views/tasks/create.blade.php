
@extends('layout')

@section('content')

<div class="col-md-8 col-sm-8 col-xs-12">
    <div class="main-content">
        <article>
            @if($task->id)
                <h4 class="page-title mt-1">
                    Update task
                    <a target="_blank" href="{{route('task.show', $task->id)}}">show</a>
                </h4>
            @else
                <h4 class="page-title mt-1">Create new task</h4>
            @endif

            <div class="form-body">
                <form action="{{$task->id ? route('task.update', ['id' => $task->id]) : route('task.store')}}" method="POST">
                    @csrf

                    @if($task->id)
                        @method('PUT')
                    @endif

                    <div class="form-group">
                        <label for="exampleInputText1">Title</label>
                        <input type="text" class="form-control" name="title" value="{{$task->title}}">
                        @if ($errors->has('title'))
                            <span class="help-block alert-warning">{{ $errors->first('title') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="overview">Task overview</label>
                        <textarea name="overview" class="form-control" rows="5">{{ $task->overview }}</textarea>
                        @if ($errors->has('overview'))
                            <span class="help-block alert-warning">{{ $errors->first('overview') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Task scope</label>
                        <textarea name="text" class="form-control" rows="20" placeholder="">{{$task->text}}</textarea>
                        @if ($errors->has('text'))
                            <span class="help-block alert-warning">{{ $errors->first('text') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Task footer</label>
                        <textarea name="footer" class="form-control" rows="5" placeholder="">{{$task->footer}}</textarea>
                        @if ($errors->has('footer'))
                            <span class="help-block alert-warning">{{ $errors->first('footer') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" name="price" class="form-control" id="exampleInputEmail1" value="{{$task->price}}">
                        @if ($errors->has('price'))
                            <span class="help-block alert-warning">{{ $errors->first('price') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tags</label>
                        <input type="text" name="tags" class="form-control" id="exampleInputEmail1" value="{{$task->tags}}">
                        @if ($errors->has('tags'))
                            <span class="help-block alert-warning">{{ $errors->first('tags') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="deadline">Deadline date</label>
                        <input type="text" class="form-control" id="deadline" name="deadline" value="{{$task->deadline}}" autocomplete="off">
                        @if ($errors->has('deadline'))
                            <span class="help-block alert-warning">{{ $errors->first('deadline') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>

                        <select name="status" id="" class="form-control">
                            <option @if($task->status == 'draft') selected @endif  value="draft">Draft</option>
                            <option @if($task->status == 'open') selected @endif value="open">Open</option>
                            <option @if($task->status == 'assigned') selected @endif value="assigned">Assigned</option>
                            <option @if($task->status == 'closed') selected @endif value="closed">Closed</option>
                        </select>

                    </div>

                    <button type="submit" class="btn btn-success form-btn">
                        @if($task->id) Update @else Create @endif
                    </button>

                    <div>
                        <h4>Article example</h4>
                        <p>line1</p>
                        <p>=== 1 2 44</p>
                        <p>line2</p>
                        <p>=== 2,4,6</p>
                    </div>
                </form>
            </div>
        </article>
    </div><!-- main-content -->
</div>


<div class="col-md-4 col-sm-4 col-xs-12">
    <div class="main-content">

        <h4>Wiki <a target="_blank" href="/wiki">list</a></h4>
        <ul>
            @foreach($wikies as $wiki)
                <li>
                    {{$wiki->id}} -
                    <a target="_blank" href="{{$wiki->link}}">
                        {{$wiki->title}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#deadline').datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection

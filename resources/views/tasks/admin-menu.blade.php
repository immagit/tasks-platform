@auth
    @if (Auth::user()->isAdmin())
        <div class="admin-menu alert-info">
            <a class="btn btn-secondary btn-xs" href="{{route('task.edit', ['id' => $task->id])}}">Edit</a>
        </div>
    @endif
@endauth
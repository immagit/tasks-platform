

@extends('layout')


@section('content')
    <table class="table">
        <thead>
            <th>id</th>
            <th>title</th>
            <th>link</th>
            <th>actions</th>
        </thead>

        <tbody>
            @foreach($wikies as $wiki)
                <tr>
                    <td>{{$wiki->id}}</td>
                    <td>{{$wiki->title}}</td>
                    <td>{{$wiki->link}}</td>
                    <td>
                        <form action="{{route('wiki.destroy', $wiki->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="X" onclick="return confirm('Are you sure?')">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <hr>

    <h4>Create new wiki</h4>
    <form action="{{route('wiki.store')}}" method="POST">
        @csrf

        <input type="hidden" name="" value="0">
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" name="title" class="form-control" value="{{''}}" autocomplete="off">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Link</label>
            <input type="text" name="link" class="form-control" value="{{''}}" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-success form-btn">
            Create
        </button>
    </form>
@endsection

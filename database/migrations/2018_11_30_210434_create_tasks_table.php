<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('title');
            $table->string('tags')->nullable();
            $table->longText('overview');
            $table->longText('text');
            $table->longText('footer');
            $table->decimal('price')->default(0);
            $table->date('deadline');
            $table->enum('status', ['open', 'draft', 'finished', 'closed', 'assigned']);

            $table->softDeletes();
            $table->integer('assignee_id')->nullable()->references('id')->on('users');

        });

        Schema::create('task_user', function (Blueprint $table) {

            $table->integer('task_id');
            $table->integer('user_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('task_user');
    }
}

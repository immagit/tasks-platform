<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(7),
        'overview' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'text' => implode("\n", [
            $faker->realText($maxNbChars = 100, $indexSize = 2),
            '',
            $faker->realText($maxNbChars = 100, $indexSize = 2),
            ''
        ]),
        'footer' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'price' => 10,
        'deadline' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+5 days'),
        'status' => 'open'
    ];
});
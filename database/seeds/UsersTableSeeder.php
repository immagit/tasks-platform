<?php


use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'admin',
            'is_admin' => 1,
            'email' => 'admin@tasks.com',
            'password' => bcrypt('admin')
        ]);

        \App\User::create([
            'name' => 'user',
            'is_admin' => 0,
            'email' => 'user@tasks.com',
            'password' => bcrypt('user')
        ]);

        factory(App\User::class, 3)->create();
    }
}

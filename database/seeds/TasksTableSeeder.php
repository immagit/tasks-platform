<?php
/**
 * Created by PhpStorm.
 * User: rtt
 * Date: 01.12.18
 * Time: 21:03
 */

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Task::class, 10)->create();
    }
}